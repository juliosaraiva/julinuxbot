const util = require("util");
const axios = require("axios");
const Telebot = require("telebot");
const textToSpeech = require("@google-cloud/text-to-speech");
const { Translate } = require("@google-cloud/translate").v2;
const { returnKeywords } = require("./watson.js");
const profanity = require("profanity-util");
const TOKEN_ID = require('./env.json').TOKEN_ID

const checkProfanity = text => {
  const profanityWords = profanity.check(text);
  console.log(profanityWords);
  if (profanityWords.length) {
    return true;
  }

  return false;
};

const bot = new Telebot(TOKEN_ID);
const translate = new Translate();

const synthesisVoice = async text => {
  const client = new textToSpeech.TextToSpeechClient();

  const request = {
    input: { text: text },
    voice: { languageCode: "pt-BR", ssmlGender: "NEUTRAL" },
    audioConfig: { audioEncoding: "MP3" }
  };

  const [response] = await client.synthesizeSpeech(request);
  return response.audioContent;
};

const textTranslate = async text => {
  // The target language
  const target = "en";

  // translate from pt-BR to en-EN
  const [translation] = await translate.translate(text, target);
  return translation;
};

bot.on(["/say", "/Say", "/SAY"], async msg => {
  const text = msg.text.split("/say ").join(" ");
  let validText;
  const badWords = await checkProfanity(text);
  if (badWords) {
    validText = "Não posso dizer palavrão nessa porra.";
  } else {
    validText = text;
  }
  voice = await synthesisVoice(validText);
  return bot.sendVoice(msg.chat.id, voice);
});

bot.on(["/dolar"], async msg => {
  const user = msg.from.username;
  bot.sendMessage(msg.chat.id, "Analisando a cotação do dólar...", {
    replyToMessage: msg.message_id
  });
  const fetchDolar = await axios.get(
    "https://economia.awesomeapi.com.br/all/USD-BRL"
  );
  const dolar = fetchDolar.data.USD.bid;
  const text = `Olá ${user}! O dólar hoje está valendo R$${parseFloat(
    dolar.replace(",", ".")
  ).toFixed(2)}`;
  const voice = await synthesisVoice(text);
  return bot.sendVoice(msg.chat.id, voice);
});

bot.on(["/translate"], async msg => {
  const text = msg.text.split("/translate ").join(" ");
  const translateToEnglish = await textTranslate(text);
  return bot.sendMessage(msg.chat.id, translateToEnglish, {
    replyToMessage: msg.message_id
  });
});

bot.on(["/keywords"], async msg => {
  const text = msg.text;
  const keywords = await returnKeywords(text);
  console.log(msg.chat.id)
  return bot.sendMessage(msg.chat.id, keywords);
});

bot.start();
