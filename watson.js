const { promisify } = require("util");
const apikey = require("./credentials/watson-nlu.json").apikey;
const NaturalLanguageUnderstandingV1 = require("ibm-watson/natural-language-understanding/v1");
const { IamAuthenticator } = require("ibm-watson/auth");

const naturalLanguageUnderstanding = new NaturalLanguageUnderstandingV1({
  version: "2019-07-12",
  authenticator: new IamAuthenticator({
    apikey: apikey
  }),
  url: "https://gateway.watsonplatform.net/natural-language-understanding/api"
});

module.exports.returnKeywords = async sentence => {
  const response = await naturalLanguageUnderstanding.analyze({
    text: sentence,
    features: {
      keywords: {}
    }
  });

  return JSON.stringify(response.result.keywords);
};
